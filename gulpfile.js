'use strict';

const gulp = require('gulp'),
      pug = require('gulp-pug'),
      sass = require('gulp-sass'),
      uglify = require('gulp-uglify'),
      plumber = require('gulp-plumber'),
      imagemin = require('gulp-imagemin'),
      autoprefixer = require('gulp-autoprefixer'),
      browserSync = require('browser-sync').create(),
      reload = browserSync.reload;

// SERVE ///////////////////////////////////////////////////////////////
gulp.task('browser-sync', ['views', 'styles', 'imgmin'], () => {
  browserSync.init({
    server: {
      baseDir: './build'
    }
  });

  gulp.watch('./source/*.html', ['views']);
  gulp.watch('./source/sass/**/*.scss', ['styles']);
});

// VIEWS ///////////////////////////////////////////////////////////////
gulp.task("views", () => {
  return gulp.src("./source/*.html")
      .pipe(gulp.dest("./build"))
      .on("end", reload);
});


// STYLES //////////////////////////////////////////////////////////////
gulp.task('styles', () => {
  return gulp.src('./source/sass/*.scss')
    .pipe(plumber({
      errorHandler: function(error) {
        console.log(error.message);
        this.emit('end');
      }
    }))
    .pipe(sass({outputStyle: 'compressed'}))
    .pipe(autoprefixer())
    .pipe(gulp.dest('./build/assets/css/'))
    .pipe(browserSync.stream());
});

// IMAGEMIN ////////////////////////////////////////////////////////////
gulp.task("imgmin", () => {
  gulp.src("./source/img/**/*")
    .pipe(imagemin({
      optimizationLevel: 3,
      progressive: true,
      interlaced: true
    }))
    .pipe(gulp.dest("./build/assets/img/"));
});


// DEFAULT TASK ////////////////////////////////////////////////////////
gulp.task('default', ['browser-sync']);
